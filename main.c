#include <stdio.h>
#include <string.h>
#include "memb.h"

struct cTinyString {
   char data[64];
 };

int main(int argc, char **argv)
{
    printf("hello\n");

    MEMB(allcTinyStrings, struct cTinyString, 1);
    memb_init(&allcTinyStrings);

    struct cTinyString *myString1;
    myString1 = memb_alloc(&allcTinyStrings);
    if(myString1 == NULL) {
        printf("memb_alloc failed\n");
    }
    printf("mystring1 addr: %p\n", (void*)myString1);
    printf("mystring1->data addr: %p\n", (void*)myString1->data);
    strcpy(myString1->data,"my first string");
    printf("mystring1: %s\n", myString1->data);

    memb_free(&allcTinyStrings, myString1);

    struct cTinyString *myString2;
    myString2 = memb_alloc(&allcTinyStrings);
    if(myString2 == NULL) {
        printf("memb_alloc failed\n");
    }
    printf("mystring2 addr: %p\n", (void*)myString2);
    strcpy(myString2->data,"my second string");
    printf("mystring2: %s\n", myString2->data);

    printf("goodbye\n");

    return 0;
}